import java.util.ArrayList

fun levenshteinDistance(chaine1 : CharSequence, chaine2 : CharSequence) : Int {
    val chaine1Length = chaine1.length
    val chaine2Length = chaine2.length

    var cost = IntArray(chaine1Length + 1) { it }
    var newCost = IntArray(chaine1Length + 1) { 0 }

    for (i in 1..chaine2Length) {
        newCost[0] = i

        for (j in 1..chaine1Length) {
            val editCost= if(chaine1[j - 1] == chaine2[i - 1]) 0 else 1

            val costReplace = cost[j - 1] + editCost
            val costInsert = cost[j] + 1
            val costDelete = newCost[j - 1] + 1

            newCost[j] = minOf(costInsert, costDelete, costReplace)
        }

        val swap = cost
        cost = newCost
        newCost = swap
    }
    return cost[chaine1Length]
}

fun percentMatch(chaine1:String,chaine2:String):Int{
    val distance = levenshteinDistance(chaine1,chaine2)
    val proxim = 100-(distance.toFloat() * 100)/(chaine1.length + chaine2.length)
    return  proxim.toInt()
}

fun top10__ (chaine:String,list:List<String>):List<String>{
    val liste_top10 = list.distinct()
    val map:MutableMap<String,Int> = mutableMapOf()
    for(i in liste_top10){
        var proxim =percentMatch(chaine,i)
        map.put(i,proxim)
    }

    val map10 = map.toList().sortedByDescending{ (_,value)->value }.take(10)
    val top10:MutableList<String> = mutableListOf()
    for(e in map10){
        top10.add(e.first)
    }
    return top10
}

fun top10(chaine:String,list:List<String>):List<String>{
    var top10 = list.associateBy ({it} , {percentMatch(it,chaine)} ).toList()
        .sortedByDescending { (_, value) -> value}.take(10)
        .map { it.first }
        .toList()
    return top10
}