import Kotlin.DataLoader.load
import Kotlin.DataLoader.loadUniqueFirstnames


fun main(){
    println("Hello")
    val c1 = "OLIVIER"
    val c2 = "Julien"
    val c3 = "Julie"
    val fileName = "nat2017.txt"
    val liste = loadUniqueFirstnames(fileName).forEach { println(it) }
    /*18
    val top10 = top10(c1,liste)
    for(i in top10) {
        println(i)
    } */

    /* 22
    val collection = load(fileName)
    for (c in collection){
        println(c.preusuel)
    }*/
    /* 23
    val col = load(fileName)
    val collection = col.groupBy { it.preusuel }
    for (c in collection){
        if(c.key.contains("OLIVIER")){
            println(c.key)
        }
    }*/
    /* 24
    val col = load(fileName).groupBy { it.preusuel }//.mapValues { it.value.map {it.annais + ":" +it.nombre}.sorted()}
    val olivier = "OLIVIER"
    val collection = top10(olivier,col.keys.toList()).forEach{
        println(it)
        if(col.containsKey(it)){
            col.getValue(it).sortedByDescending{ it.nombre }.forEach {
                println("\t"+it.annais+":"+it.nombre)
            }
        }
    }*/
    val col = load(fileName).groupBy { it.preusuel }
    val olivier = "OLIVIER"
    val collection = top10(olivier,col.keys.toList()).forEach{
        if(col.containsKey(it)){
            val annee = col.getValue(it).filter { it.annais !="XXXX" }.maxBy { it.nombre }
            println(it+":"+annee?.annais+","+annee?.nombre ?: "aucun")
        }
    }


}

