import java.io.FileReader

class Kotlin {
    object DataLoader {

        fun loadUniqueFirstnames(nameFile:String):List<String>{
            val liste_prenoms:MutableSet<String> = mutableSetOf()
            liste_prenoms.toSet()
            FileReader(nameFile).use {
                it.forEachLine {
                    val prenom = it.split("\t")[1]
                    if(prenom.length>=2)liste_prenoms.add(prenom)
                }
            }
            liste_prenoms
            return liste_prenoms.toList()
        }

        fun load(path:String):Collection<Firstname>{
            val collection:MutableCollection<Firstname> = mutableSetOf()
            var i=0
            FileReader(path).use {
                it.forEachLine {
                    if(i!=0) {
                        val sexe = it.split("\t")[0].toInt()
                        val prenom = it.split("\t")[1]
                        val annee = it.split("\t")[2]
                        val nombre = it.split("\t")[3].toInt()
                        val f = Firstname(sexe,prenom,annee,nombre)
                        collection.add(f)
                    }
                    i++
                }
            }
            return collection
            }
    }
}