import Kotlin.DataLoader.loadUniqueFirstnames
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable

class TextToolsTests {

    @Test fun levenshteinDistanceTest(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(6, levenshteinDistance("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(1, levenshteinDistance("Julie", "Julien")) }
        )
    }

    @Test fun percentMatch(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(50, percentMatch("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(90, percentMatch("Julie", "Julien")) }
        )
    }

    @Test fun top10(){
        val fileName = "nat2017.txt"
        val liste = loadUniqueFirstnames(fileName)
        val liste_res = listOf<String>("OLIVIER","OLIVIERO","OLLIVIER","LIVIER","OLIVER","OLIVIEN","OLIWIER","OLIVERA","OLIVINE","OLIVIA")

        Assertions.assertAll(
            Executable { Assertions.assertEquals(liste_res, top10("OLIVIER", liste)) }
        )
    }
}